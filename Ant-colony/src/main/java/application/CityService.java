package application;

import java.util.List;
import java.util.Random;

import model.City;

public class CityService {
	public static void generateCities(List<City> cities, int n) {
		Random generator = new Random();
		for (int i = 0; i < n; i++) {
			cities.add(new City(generator.nextInt(n*100), generator.nextInt(n*100)));
		}
	}
}
