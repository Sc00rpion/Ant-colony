package application;

import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import model.Ant;
import model.City;

public class Algorithm {
	private final List<City> cities;
	private Ant[] ants;
	private double[][] distance;
	private double[][] pheromones;
	private Random random = new Random();
	private double bestTourLength;
	private int[] bestTour;

	private double randomness = 0.2;
	private double evaporation = 0.5;
	private double alpha = 1;
	private double beta = 3;
	private double Q = 50;
	private int initialPheromones = 100;
	private int iteration;

	public Algorithm(List<City> cities, int antsNumber, int iteration) {
		this.cities = Collections.unmodifiableList(new ArrayList<City>(cities));
		pheromones = new double[cities.size()][cities.size()];
		distance = new double[cities.size()][cities.size()];
		ants = new Ant[antsNumber];
		this.iteration = iteration;
		init();

	}

	public void run() {
		int x = 0;
		while (x < iteration && !isSufficientSolution()) {
			generateSolution();
			updatePheromones();
			updateBestSolution();
			if (x < 10 || x % 10 == 0 || x == iteration - 1) {
				System.out.printf("Iteracja: %d Najkrótsza droga to: %.2f \n", x, bestTourLength);
			}
			x++;
		}
	}

	// Inicjalizacja: mrówki, feromony, liczenie dystansu pomiędzy miastami

	private void init() {
		initAnts(cities.size());
		calculateDistance();
		initPheromones();
	}

	private void initAnts(int size) {
		for (int i = 0; i < ants.length; i++) {
			ants[i] = new Ant(size);
		}
	}

	private void initPheromones() {
		for (int i = 0; i < pheromones.length; i++) {
			for (int j = 0; j < pheromones.length; j++) {
				pheromones[i][j] = initialPheromones;
			}
		}
	}

	private void calculateDistance() {
		for (int i = 0; i < distance.length; i++) {
			for (int j = 0; j < distance.length; j++) {
				if (i == j) {
					distance[i][i] = 0;
				} else {
					double d = sqrt(Assembler.pow(cities.get(j).getX() - cities.get(i).getX(), 2)
							+ Assembler.pow(cities.get(j).getY() - cities.get(i).getY(), 2));
					distance[i][j] = d;
				}
			}
		}

	}

	// Aktualizacja feromonów na ścieżkach pomiędzy miastami -----------

	private void updatePheromones() {
		for (int i = 0; i < pheromones.length; i++) {
			for (int j = 0; j < pheromones.length; j++) {
				pheromones[i][j] *= evaporation;
			}
		}
		for (int i = 0; i < ants.length; i++) {
			updatePheromones(ants[i]);
		}

	}

	private void updatePheromones(Ant ant) {
		double contribution = Q / lengthTour(ant);
		int[] tour = ant.getTour();
		pheromones[tour[tour.length - 1]][tour[0]] += contribution;
		for (int i = 0; i < tour.length - 1; i++) {
			pheromones[tour[i]][tour[i + 1]] += contribution;
		}
	}
	// Generowanie rozwiązania dla pojedyńczej iteracji -------------

	private void generateSolution() {

		ForkGenerateSolution fgs = new ForkGenerateSolution(ants);
		ForkJoinPool pool = new ForkJoinPool();
		pool.invoke(fgs);

	}

	private void generateSolution(Ant[] ants) {
		for (int i = 0; i < ants.length; i++) {
			generateSolution(ants[i]);
		}
	}

	private void generateSolution(Ant ant) {
		ant.clear();
		ant.visitCity(selectNextCityUsingLottery(ant));
		for (int i = 0; i < cities.size() - 1; i++) {
			ant.visitCity(selectNextCity(ant));
		}
	}

	private int selectNextCity(Ant ant) {
		if (randomness > random.nextDouble()) {
			return selectNextCityUsingLottery(ant);
		} else {
			return selectNextCityUsingProbability(ant);
		}
	}

	private int selectNextCityUsingProbability(Ant ant) {
		int[] citiesId = new int[cities.size()];
		double[] probability = new double[cities.size()];
		int count = 0;
		double total = 0;
		for (int i = 0; i < cities.size(); i++) {
			if (!ant.visited(i)) {
				int last = ant.getLastCity();
				double pher = getPheromone(last, i);
				double dist = 1 / getDistance(last, i);
				double a = Assembler.pow(pher, (int) alpha);
				double b = Assembler.pow(dist, (int) beta);
				double prob = a * b;
				citiesId[count] = i;
				probability[count] = prob;
				count++;
				total += prob;
			}
		}
		for (int i = 0; i < count; i++) {
			probability[i] = probability[i] / total;
		}
		double rand = random.nextDouble();
		double cumulativeProbability = 0;
		for (int i = 0; i < count; i++) {
			cumulativeProbability += probability[i];
			if (rand <= cumulativeProbability) {
				return citiesId[i];
			}
		}
		return -1;
	}

	private int selectNextCityUsingLottery(Ant ant) {
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < cities.size(); i++) {
			if (!ant.visited(i)) {
				list.add(i);
			}
		}
		return list.get(random.nextInt(list.size()));
	}
	
	// Klasa do generowania rozwiązania równolegle ---------------------

	private class ForkGenerateSolution extends RecursiveAction {
		private Ant[] ants;

		public ForkGenerateSolution(Ant[] ants) {
			this.ants = ants;
		}

		@Override
		protected void compute() {
			if (ants.length < 10) {
				generateSolution(ants);
			} else {
				int mid = ants.length / 2;
				invokeAll(new ForkGenerateSolution(Arrays.copyOfRange(ants, 0, mid)),
						new ForkGenerateSolution(Arrays.copyOfRange(ants, mid, ants.length)));
			}

		}

	}

	// Aktualizacja najlepszego rozwiązania -----------------
	// TODO można zrobić podczas aktualizacji feromonów (nie trzeba będzie
	// liczyć 2 raz)

	private void updateBestSolution() {
		if (bestTour == null) {
			bestTour = ants[0].getTour();
			bestTourLength = lengthTour(ants[0]);
		}
		for (int i = 0; i < ants.length; i++) {
			if (lengthTour(ants[i]) < bestTourLength) {
				System.out.printf("Znaleziono lepsze rozwiązanie: %.2f \n", lengthTour(ants[i]));
				bestTourLength = lengthTour(ants[i]);
				bestTour = ants[i].getTour().clone();
			}
		}
	}

	private double getPheromone(int city1, int city2) {
		return pheromones[city1][city2];
	}

	private double getDistance(int city1, int city2) {
		return distance[city1][city2];
	}



	private boolean isSufficientSolution() {
		// TODO Auto-generated method stub
		return false;
	}

	private double lengthTour(Ant ant) {
		int[] tour = ant.getTour();
		double length = distance[tour[tour.length - 1]][tour[0]];
		for (int i = 0; i < tour.length - 1; i++) {
			length += distance[i][i + 1];
		}
		return length;
	}

}
