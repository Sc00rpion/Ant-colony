package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import model.City;

public class CityIO {
	public static void saveCities(List<City> cities, String output) {
		FileOutputStream fos;
		ObjectOutputStream oos;
		try {
			fos = new FileOutputStream(output);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(cities);
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<City> readCities(String input) {
		FileInputStream fis;
		ObjectInputStream ois;
		List<City> cities = new ArrayList<>();
		try {
			fis = new FileInputStream(input);
			ois = new ObjectInputStream(fis);
			cities = (List<City>) ois.readObject();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cities;
	}

}
