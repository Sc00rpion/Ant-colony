package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import model.City;
import validator.PositiveInteger;

public class Application {

	@Parameter(description = "Liczba miast do wygenerowania", names = { "-g", "-generate" } , validateWith = PositiveInteger.class)
	int generate = 0;
	@Parameter(description = "Nazwa pliku do którego będzie zapisana kopia danych", names = { "-o" })
	String output = null;
	@Parameter(description = "Nazwa pliku z którego będzie wczytana kopia danych", names = { "-d" })
	String input = null;
	@Parameter(description = "Liczba mrówek wykorzystana w algorytmie", names = { "-a", "-ant" }, validateWith = PositiveInteger.class)
	int ant = 100;
	@Parameter(description = "Liczba iteracji do wykonania w algorytmie", names = { "-i", "-iteration" }, validateWith = PositiveInteger.class)
	int iteration = 300;
	@Parameter(names = { "-h", "-help" }, description = "Opis użycia")
	private boolean help = false;

	public static void main(String[] args) throws IOException {
		
		Application main = new Application();
		JCommander com = JCommander.newBuilder().addObject(main).build();
		try {
			com.parse(args);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			com.usage();
			System.exit(1);
		}
		if (main.help) {
			com.usage();
			System.exit(1);
		}
		if (main.input != null) {
			File inputFile = new File(main.input);
			if (!inputFile.exists()) {
				System.err.println("Nie znaleziono pliku: " + inputFile.getAbsolutePath());
				com.usage();
				System.exit(1);
			}
		}

		main.run();
	}

	public void run() throws IOException {
		long start = System.currentTimeMillis();
		List<City> cities = new ArrayList<>();
		
		if(input != null){
			cities = CityIO.readCities(input);
		}
		if(generate != 0){
			CityService.generateCities(cities, generate);
		}
		if(output != null){
			CityIO.saveCities(cities, output);
		}
		if(cities.isEmpty()){
			System.exit(0);
		}
		

		Algorithm alg = new Algorithm(cities, ant, iteration);
		alg.run();
		long stop = System.currentTimeMillis();
		System.out.println("Czas wykonania (w milisekundach): " + (stop - start));
	}





}
