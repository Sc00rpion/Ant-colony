package validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PositiveInteger implements IParameterValidator{

	@Override
	public void validate(String name, String value) throws ParameterException {
	    int n = Integer.parseInt(value);
	    if (n < 0) {
	      throw new ParameterException("Parametr " + name + " powinien być dodatni (wczytano " + value +")");
	    }
	}

}
