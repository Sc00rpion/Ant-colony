package model;

import java.util.Arrays;

public class Ant {
	private boolean[] visited;
	private int[] tour;
	private int index;

	public Ant(int numberOfCities) {
		visited = new boolean[numberOfCities];
		Arrays.fill(visited, false);
		tour = new int[numberOfCities];
		index = 0;
	}
	public void clear(){
		Arrays.fill(visited, false);
		index = 0;
	}
	public int[] getTour() {
		return tour;
	}
	public void visitCity(int city){
		tour[index++] = city;
		visited[city] = true;
	}
	public boolean visited(int city){
		return visited[city];
	}
	public int getLastCity(){
		return tour[index - 1];
	}

}
