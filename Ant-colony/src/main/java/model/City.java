package model;

import java.io.Serializable;

public class City implements Serializable {
	private static int nextId = 0;

	private int id;
	private int x;
	private int y;
	
	public City(int x, int y){
		id = nextId++;
		this.x = x;
		this.y = y;
	}
	public int getId() {
		return id;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
